var Room = require( 'colyseus' ).Room;
const {
    addPlayer,
    addRoom,
    addRound,
    deleteRoom,
    getRoom,
    getRooms
} = require( './models/chatRoom' );
const cluster = require( "cluster" );
const util = require( 'util' );

const notifications = {
    WillClientReady: "ColyseusClient.WillClientReady",
    DidClientReady: "ColyseusClient.DidClientReady",
    WillClientSelected: "ColyseusClient.ClientSelected",
    DidClientSelected: "ColyseusClient.DidClientSelected",
    WillClientSelectedReady: "ColyseusClient.WillClientSelectedReady",
    DidClientSelectedReady: "ColyseusClient.DidClientSelectedReady",
    BattleStart: "ColyseusClient.BattleStart",
    WillClientBeginRound: "RoundBeginState.WillClientBeginRound",
    DidClientBeginRound: "RoundBeginState.DidClientBeginRound",
    WillSubmitCard: "ConfirmCardState.WillSubmitCard",
    WillGetCurrentPlayer: "CheckPlayState.WillGetCurrentPlayer",
    WillMove: "MoveTargetState.WillMove",
    WillMoveIndicator: "BattleState.WillMoveIndicator",
    WillAction: "PerformAbilityState.WillAction",
    WillEndFacing: "EndFacingState.WillEndFacing",
    WillEndTurn: "ColyseusClient.WillEndTurn",
    WillAddCard: "ColyseusClient.WillAddCard"
}

class ChatRoom extends Room {

    constructor() {
        super();

        this.setState( {
            players: {},
            start_date: new Date(),
            end_date: new Date(),
            current_player: false,
            current_turn: 0,
            current_round: 0,
            location: [],
            prioritizeCard: [],
            server: {
                isReady: false,
                isSelected: false,
                isBeginRound: false,
                prioritizeCard: false,
                isEndTurn: false,
                resetRound: false
            },
            users: []
        } );
    }

    onInit( options ) {
        this.options = options;
        this.roomName = options.roomName;

        for ( let i = 1; i <= 25; i++ ) {
            this.state.location.push( i );
        }

        this.setPatchRate( 1000 / 20 );
        this.setSimulationInterval( this.update.bind( this ) );

        console.log( "ChatRoom created!", options );
    }

    requestJoin( options ) {
        console.log( "request join!", options );
        console.log( "requestJoin", this.clients.length < this.options.maxClients );
        if ( this.clients.length < this.options.maxClients ) {
            let user = {
                username: options.username,
                rank: options.rank,
                avatarId: options.avatarId,
                clientId: options.clientId
            }
            this.state.users.push( user );
        }
        return this.clients.length < this.options.maxClients;
    }

    onJoin( client ) {
        console.log( "client joined!", client.id );

        let user = this.state.users.filter( item => {
            return item.clientId == client.id;
        } );

        if ( user.length == 1 ) {
            let random = Math.floor( Math.random() * this.state.location.length );
            this.state.location.splice( random, 1 );

            this.state.players[ client.id ] = {
                location: random,
                face: Math.floor( Math.random() * 5 ),
                position: 0,
                action: 0,
                direction: 0,
                indicator: 0,
                tribe: {
                    tribeId: 1,
                },
                selectCard: 0,
                isReady: false,
                isSelected: false,
                isBeginRound: false,
                isSelectedCard: false,
                isBeginTurn: false,
                isEndTurn: false,
                isAddCard: false
            };

            // this.state.server.cards[ client.id ] = 0; // init card id

            addPlayer( this.roomName, client.id, this.state.players[ client.id ].location, this.state.players[ client.id ].face, user[ 0 ].username, user[ 0 ].rank, user[ 0 ].avatarId ).then( i => {
                console.log( i );
            } );
        }
    }

    onLeave( client ) {
        console.log( "client left!", client.id );

        // to add location value back
        let location = this.state.players[ client.id ].location;
        this.state.location.push( location );

        // delete chatroom in db
        deleteRoom( this.roomName, client.id ).then( i => {
            console.log( i );
        } )

        delete this.state.players[ client.id ];
    }

    onMessage( client, data ) {
        console.log( data, "received from", client.id );
        // this.state.messages.push( client.id + " sent " + data );
        let args = data.split( ' ' );

        switch ( args[ 0 ] ) {
            case notifications.WillClientReady:
                this.state.players[ client.id ].isReady = !this.state.players[ client.id ].isReady;
                console.log( client.id, "isReady", this.state.players[ client.id ].isReady );
                this.state.server.isReady = this.clients.length > 1 && this.CheckAllClientReady();
                break;
            case notifications.DidClientSelected:
                let id = '';
                if ( args.length > 1 ) {
                    let data = args[ args.length - 1 ].split( '' );
                    id = data[ data.length - 1 ]
                }

                console.log( client.id, "tribeId", +id );

                this.state.players[ client.id ].tribe.tribeId = +id;
                break;
            case notifications.WillClientSelectedReady:
                this.state.players[ client.id ].isSelected = true;
                console.log( client.id, "isSelected", this.state.players[ client.id ].isSelected );
                this.state.server.isSelected = this.clients.length > 1 && this.CheckAllClientSelected();
                break;
            case notifications.WillClientBeginRound:
                this.state.players[ client.id ].isBeginRound = true;
                console.log( client.id, "isBeginRound", this.state.players[ client.id ].isBeginRound );
                this.state.server.isBeginRound = this.clients.length > 1 && this.CheckAllClientBeginRound();
                break;
            case notifications.DidClientBeginRound:
                this.state.players[ client.id ].isBeginRound = false;
                this.state.server.isBeginRound = false;
                break;
            case notifications.WillSubmitCard:
                if ( args.length > 1 ) {
                    let priority = args[ 1 ];
                    this.state.players[ client.id ].selectCard = +priority;
                    this.state.players[ client.id ].isSelectedCard = true;

                    if ( this.CheckAllClientSelectedCard() ) {
                        this.CalculatePriority()
                    }
                }
                break;
            case notifications.WillGetCurrentPlayer:
                this.state.players[ client.id ].isBeginTurn = true;
                console.log( client.id, "isBeginTurn", this.state.players[ client.id ].isBeginTurn );
                console.log( "CheckAllBeginTurn", this.CheckAllBeginTurn(), "this.CheckEndRound()", this.CheckEndRound() );
                if ( this.CheckAllBeginTurn() ) {
                    if ( this.CheckEndRound() ) {
                        this.ResetTurn();

                        // this.state.current_player = this.state.prioritizeCard[ this.state.current_turn ].playerId;
                        this.state.current_turn++;
                        console.log( 'WillGetCurrentPlayer', this.state.current_turn, this.state.current_player );
                        // some code to update room in db
                        // change this.state.current_player = false;
                    }
                    else {
                        this.ResetRoundStatus();
                    }
                }
                break;
            case notifications.WillMove:
                if ( args.length > 2 ) {
                    let x = args[ 1 ];
                    let y = args[ 2 ];
                    let position = '1' + x + '1' + y + '1';
                    this.state.players[ client.id ].position = +position;
                }
                break;
            case notifications.WillAction:
                if ( args.length > 4 ) {
                    let x = args[ 1 ];
                    let y = args[ 2 ];
                    let totalAttack = args[ 3 ];
                    let targetAbility = args[ 4 ];
                    let action = '1' + x + '1' + y + '1' + totalAttack + '1' + targetAbility + '1';
                    this.state.players[ client.id ].action = +action;
                }
                break;
            case notifications.WillMoveIndicator:
                if ( args.length > 2 ) {
                    let x = args[ 1 ];
                    let y = args[ 2 ];
                    let indicator = '1' + x + '1' + y + '1';
                    this.state.players[ client.id ].indicator = +indicator;
                }
                break;
            case notifications.WillEndFacing:
                if ( args.length > 1 ) {
                    let direction = args[ 1 ];
                    this.state.players[ client.id ].direction = +( '1' + direction );
                }
                break;
            case notifications.WillEndTurn:
                if ( args.length > 0 ) {
                    this.state.players[ client.id ].isEndTurn = true;
                    this.state.players[ client.id ].isBeginTurn = false;
                    this.state.server.isEndTurn = this.CheckEndTurn();
                }
                break;
            case notifications.WillAddCard:
                if ( args.length > 0 ) {
                    this.state.players[ client.id ].isAddCard = true;
                    if ( this.CheckAllAddCard() ) {
                        this.state.server.prioritizeCard = true;
                    }

                }
                break;
            default:
                break;
        }
    }

    update() {
        // console.log("num clients:", Object.keys(this.clients).length);
        // for ( var id in this.state.players ) {
        //     this.state.players[ id ].position.x++;
        // }
    }

    onDispose() {
        console.log( "Dispose ChatRoom" );
    }

    // implementation methods
    CheckAllAddCard() {
        let isAddCard = true;
        for ( var id in this.state.players ) {
            isAddCard = isAddCard && this.state.players[ id ].isAddCard;
        }

        return isAddCard;
    }

    ResetTurn() {
        this.state.server.resetRound = false;
        this.state.server.isEndTurn = false;

        for ( var id in this.state.players ) {
            this.state.players[ id ].isEndTurn = false;
        }
    }

    ResetRoundStatus() {
        console.log( 'ResetRoundStatus' );
        this.state.current_turn = 0;
        this.state.current_player = false;
        this.state.prioritizeCard = [];

        this.state.server.isBeginRound = false;
        this.state.server.isSelectedCard = false;
        this.state.server.isEndTurn = false;
        this.state.server.prioritizeCard = false;

        for ( var id in this.state.players ) {
            this.state.players[ id ].isBeginTurn = false;
            this.state.players[ id ].isEndTurn = false;
            this.state.players[ id ].isSelectedCard = false;
            this.state.players[ id ].isBeginRound = false;
            this.state.players[ id ].isAddCard = false;
            this.state.players[ id ].direction = 0;
            this.state.players[ id ].action = 0;
            this.state.players[ id ].position = 0;

            // this.state.players[ id ].selectCard = 0;
        }

        this.state.server.resetRound = true;
    }

    CheckEndTurn() {
        let isEndTurn = true;
        for ( var id in this.state.players ) {
            isEndTurn = isEndTurn && this.state.players[ id ].isEndTurn;
        }

        return isEndTurn;
    }

    CheckEndRound() {
        return this.state.current_turn < Object.keys( this.state.players ).length;
    }

    CheckAllBeginTurn() {
        let beginTurn = true;
        for ( var id in this.state.players ) {
            beginTurn = beginTurn && this.state.players[ id ].isBeginTurn;
        }
        return beginTurn;
    }

    CheckAllClientReady() {
        let ready = true;
        for ( var id in this.state.players ) {
            ready = ready && this.state.players[ id ].isReady;
        }
        return ready;
    }

    CheckAllClientSelected() {
        let selected = true;
        for ( var id in this.state.players ) {
            selected = selected && this.state.players[ id ].isSelected;
        }

        return selected;
    }

    CheckAllClientBeginRound() {
        let beginRound = true;
        for ( var id in this.state.players ) {
            beginRound = beginRound && this.state.players[ id ].isBeginRound;
        }

        if ( beginRound ) {
            this.state.current_round = this.state.current_round + 1;
            return beginRound;
        }
        else {
            return beginRound;
        }
    }

    CheckAllClientSelectedCard() {
        console.log( 'CheckAllClientSelectedCard' );
        let selectedCard = true;
        for ( var id in this.state.players ) {
            selectedCard = selectedCard && this.state.players[ id ].isSelectedCard;
        }

        return selectedCard;
    }

    CalculatePriority() {
        console.log( 'CalculatePriority' );
        let cardChange = [];
        for ( var id in this.state.players ) {
            let cd = {
                playerId: '',
                priority: 0

            };
            cd.playerId = id;
            cd.priority = +this.state.players[ id ].selectCard;

            cardChange.push( cd );
        }

        cardChange.sort( ( a, b ) => {
            return a.priority - b.priority;
        } ).reverse();

        let prioritizeCard = '';
        cardChange.forEach( i => {
            prioritizeCard += `${i.playerId},${i.priority} `;
        } );

        // this.state.server.prioritizeCard = true;
        this.state.prioritizeCard = cardChange;

        addRound( this.roomName, this.state.current_round, cardChange ).then( res => {
            console.log( "Server Begin Round", this.state.current_round, "!!" );
        } );
    }

}

module.exports = ChatRoom;