"use strict";

require( 'dotenv' ).config();

const colyseus = require( 'colyseus' ),
    http = require( 'http' ),
    cluster = require( "cluster" ),
    path = require( 'path' ),
    express = require( 'express' )

    ,
    bodyParser = require( 'body-parser' ),
    logger = require( 'morgan' ),
    cors = require( 'cors' ),
    helmet = require( 'helmet' )

    ,
    emitter = require( './emitter' ),
    router = require( './routes/router' ),
    connect = require( './lib/connect' ),
    util = require( 'util' ),
    ClusterMessages = require( 'cluster-messages' )

    ,
    port = process.env.PORT || 3553,
    app = express(),
    gameServer = new colyseus.Server( {
        server: http.createServer( app )
    } )

    ,
    ChatRoom = require( './chat_room' ),
    LobbyRoom = require( './lobby_room' );

const {
    addPlayer,
    addRoom,
    deleteRoom,
    getRoom,
    getRooms
} = require( './models/chatRoom' );

let messages = new ClusterMessages();

gameServer.register( 'chat', ChatRoom, {
    maxClients: 4,
    roomName: 'chat',
    password: ''
} );

gameServer.register( 'lobby', LobbyRoom, {
    maxClients: 100
} );

/* multiple worker (use in multiple core cpu server)
if ( cluster.isMaster ) {
    // console.log( cluster )
    for ( let i = 1; i < Object.keys( cluster.workers ).length; i++ ) {
        let worker = cluster.workers[ i ];

        worker.on( 'message', ( data ) => {
            if ( typeof data.id !== 'undefined' )
                for ( let j = 1; j < Object.keys( cluster.workers ).length; j++ )
                    cluster.workers[ j ].send( data );
        } );
    }
}
else {
    // event emitter for calling data 
    emitter.eventBus.on( 'createRoom', ( data ) => {
        let maxClients = 4;

        let datas = {
            id: cluster.worker.id,
            maxClients: maxClients,
            roomName: data.roomName,
            password: data.password
        }

        process.send( datas );

        addRoom( data.roomName, data.password, maxClients ).then( i => {
            console.log( i )

            data.res.json( {
                status: 200,
                message: 'ok'
            } );
        } )
    } );

    process.on( 'message', ( data ) => {
        if ( typeof data.roomName === 'undefined' )
            return;

        gameServer.register( data.roomName, ChatRoom, {
            maxClients: data.maxClients,
            roomName: data.roomName,
            password: data.password
        } )
    } );
}
*/

// single worker (use in single core cpu server)
emitter.eventBus.on( 'createRoom', ( room ) => {
    let maxClients = 4;

    let data = {
        maxClients: maxClients,
        roomName: room.roomName,
        password: room.password
    }

    if ( typeof data.roomName === 'undefined' )
        return;

    gameServer.register( data.roomName, ChatRoom, {
        maxClients: data.maxClients,
        roomName: data.roomName,
        password: data.password
    } )

    addRoom( data.roomName, data.password, maxClients ).then( i => {
        console.log( i )

        room.res.json( {
            status: 200,
            message: 'ok'
        } );
    } )
} );

// express config
app.use( logger( 'dev' ) );

app.use( bodyParser.json( {
    limit: '1mb'
} ) );
app.use( bodyParser.urlencoded( {
    limit: '1mb',
    extended: true
} ) );
app.use( cors() );
app.use( helmet() );

app.use( express.static( __dirname ) );
app.use( require( './replacer' ) );

app.use( connect.connect );
app.use( '/api/', router );
app.use( connect.close );

app.use( '/', ( req, res, next ) => {
    next( new Error( 'no method' ) );
} );

app.use( ( error, request, response, next ) => {
    response.status( error.status || 500 );
    response.json( {
        error: error.message
    } );
} );

app.use( ( request, response, next ) => {
    let error = new Error( 'Not Found' );
    error.status = 404;
    response.json( error );
} );

process.on( 'uncaughtException', function( err ) {
    console.log( err ); //Send some notification about the error
    process.exit( 1 );
} );
// end

gameServer.listen( port, 'localhost' );
console.log( 'Worker %d running on port %d', process.pid, port );