const router = require( 'express' ).Router(),
    emitter = require( '../emitter' ),
    util = require( 'util' ) // for inspec objects

const {
    addPlayer,
    addRoom,
    deleteRoom,
    getRoom,
    getRooms
} = require( '../models/chatRoom' );

router.use( '/getRooms', function( req, res, next ) {
    getRooms().then( rooms => {
        console.log( rooms )

        let data = rooms.map( room => {
            if ( room.round.length == 0 )
                return room
        } );

        data.forEach( e => {
            if ( typeof e == 'undefined' ) {
                data = [];
                return;
            }
        } );

        res.json( {
            status: 200,
            message: 'ok',
            data: data
        } );
    } ).catch( err => {
        throw err;
    } )
} );

router.use( '/getRoom', function( req, res, next ) {
    var roomName = req.body.roomName;

    getRoom( roomName ).then( rooms => {
        console.log( rooms )
        if ( rooms.length > 0 )
            res.json( {
                status: 200,
                message: 'ok',
                data: rooms
            } );
        else
            res.json( {
                status: 200,
                message: `room (${roomName}) doesn't exist!`,
                data: rooms
            } );

    } ).catch( err => {
        throw err;
    } )
} );

router.use( '/createRoom', function( req, res, next ) {
    var roomName = req.body.roomName;
    var password = req.body.password;

    emitter.eventBus.sendEvent( 'createRoom', {
        roomName,
        password,
        res
    } );
} );

module.exports = router;