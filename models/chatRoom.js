'use strict';

const r = require( 'rethinkdb' );
const connect = require( '../lib/connect' );
const conf = require( '../config/database' );

var connection = null;
r.connect( {
    host: conf.host,
    port: 28015
}, function( err, conn ) {
    if ( err ) throw err;
    connection = conn;
} )

const getRooms = () => {
    return r.db( conf.db ).table( conf.table )
        .run( connection )
        .then( ( cursor ) => {
            return cursor.toArray()
        } ).then( ( results ) => {
            return results;
        } ).catch( ( err ) => {
            throw err;
        } )
}

const getRoom = ( name ) => {
    return r.db( conf.db ).table( conf.table )
        .filter( r.row( 'name' ).eq( name ) )
        .run( connection )
        .then( ( cursor ) => {
            return cursor.toArray()
        } ).then( ( results ) => {
            return results;
        } ).catch( ( err ) => {
            throw err;
        } )
}

const addPlayer = ( name, p_id, p_location, p_face, u_name, u_rank, u_avatarId ) => {
    return getRoom( name ).then( rooms => {
        if ( !rooms.toString() )
            return Promise.resolve( `Room doesn't exist!!` );
        else
            return r.db( conf.db ).table( conf.table )
                .get( rooms[ 0 ].id )
                .update( {
                    clients: r.row( "clients" ).add( 1 ),
                    client: r.row( 'client' ).append( {
                        "id": p_id,
                        "location": p_location,
                        "face": p_face,
                        "username": u_name,
                        "rank": u_rank,
                        "avatarId": u_avatarId
                    } )
                } )
                .run( connection )
                .then( ( results ) => {
                    return results;
                } ).catch( ( err ) => {
                    throw err;
                } );
    } )
}

const addRound = ( name, round, priorities ) => {
    return getRoom( name ).then( rooms => {
        if ( !rooms.toString() )
            return Promise.resolve( `Room doesn't exist!!` );
        else
            return r.db( conf.db ).table( conf.table )
                .get( rooms[ 0 ].id )
                .update( {
                    round: r.row( 'round' ).append( {
                        "round": round,
                        "datetime": new Date(),
                        "priorities": priorities,
                        "turn": []
                    } )
                } )
                .run( connection )
                .then( ( results ) => {
                    return results;
                } ).catch( ( err ) => {
                    throw err;
                } );
    } )
}

const deleteRoom = ( name, p_id ) => {
    return getRoom( name ).then( rooms => {
        if ( !rooms.toString() )
            return Promise.resolve( `Room doesn't exist!!` );
        else {
            if ( rooms[ 0 ].clients == 1 )
                return r.db( conf.db ).table( conf.table )
                    .get( rooms[ 0 ].id )
                    .delete()
                    .run( connection )
                    .then( ( results ) => {
                        return results;
                    } ).catch( ( err ) => {
                        throw err;
                    } );
            else
                return r.db( conf.db ).table( conf.table )
                    .get( rooms[ 0 ].id )
                    .update( {
                        clients: r.row( "clients" ).sub( 1 ),
                        client: r.row( 'client' ).filter( function( item ) {
                            return item( 'id' ).ne( p_id )
                        } )
                    } )
                    .run( connection )
                    .then( ( results ) => {
                        return results;
                    } ).catch( ( err ) => {
                        throw err;
                    } );
        }
    } )
}

const addRoom = ( name, password, maxClients ) => {
    let room = Object.assign( {}, {
        'name': name,
        'password': password,
        'clients': 0,
        'maxClients': maxClients,
        'client': [],
        'round': []
    } );

    return getRoom( name ).then( i => {
        if ( i.toString() )
            return Promise.resolve( 'Room duplicate!!' );
        else
            return r.db( conf.db ).table( conf.table )
                .insert( room )
                .run( connection )
                .then( ( results ) => {
                    return results;
                } ).catch( ( err ) => {
                    throw err;
                } );
    } )
}

module.exports = {
    getRooms: getRooms,
    getRoom: getRoom,
    addPlayer: addPlayer,
    deleteRoom: deleteRoom,
    addRoom: addRoom,
    addRound: addRound
}