var Room = require('colyseus').Room;
const util = require('util') // for inspec objects
const cluster = require( "cluster" );

class LobbyRoom extends Room {

  constructor () {
    super();

    this.setState({
      rooms: []
    });
  }

  onInit (options) {
    this.options = options;
    this.setPatchRate( 1000 / 20 );
    this.setSimulationInterval( this.update.bind(this) );

    console.log("LobbyRoom created!", options);
  }

  requestJoin (options) {
    console.log("request join!", options);
    return this.clients.length < this.options.maxClients;
  }

  onJoin (client) {
    console.log("client joined!", client.id);
  }

  onLeave (client) {
    console.log("client left!", client.id);
    // delete this.state.players[client.id];
  }

  onMessage (client, data) {
    console.log(data, "received from", client.id);
  }

  update () {
    // console.log("num clients:", Object.keys(this.clients).length);
    // this.state.rooms.push();
  }

  onDispose () {
    // console.log(cluster.worker.id)
    console.log("Dispose LobbyRoom");
  }

}

module.exports = LobbyRoom;
